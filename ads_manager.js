define([
	'require',
	'madjoh_modules/styling/styling'
],
function(require, Styling){
	function AdsManager(page, adId){
		if(!page) return;

		this.page 		= page;
		this.scroller 	= page.scroller;
		this.list 		= page.list;

		this.adId 			= adId || '459967657497104_490380361122500';
		this.lastRequest 	= null;
		this.loading 		= false;
		this.retryDelay 	= 5000;

		var adsManager = this;
		this.scroller.addEventListener('scroll', function(){adsManager.update();}, false);
	}

	// Request
		AdsManager.prototype.request = function(callback, getFake){
			if(this.loading) 	return;
			if(!window.cordova || getFake) return callback(this.requestFake());

			this.loading = true;

			var adsManager = this;
			var onAdLoaded = function(data){
				adsManager.retryDelay = 5000;
				adsManager.loading = false;
				document.removeEventListener('onAdLoaded', 		onAdLoaded);
				document.removeEventListener('onAdFailLoad', 	onFailure);
				callback(data);
			};
			var onFailure = function(data){
				if(data.error.error = 1002){
					adsManager.retryDelay*=1.2;
				}

				console.log('Ad loading failed, retrying in ' + adsManager.retryDelay/1000 + 's');
				console.log('error : ' + data.error, 'reason : ' + data.reason);
				adsManager.loading = false;


				document.removeEventListener('onAdLoaded', 		onAdLoaded);
				document.removeEventListener('onAdFailLoad', 	onFailure);

				setTimeout(function(){
					adsManager.request(callback, (adsManager.retryDelay > 10000));
				}, adsManager.retryDelay);
			};

			document.addEventListener('onAdLoaded', 	onAdLoaded);
			document.addEventListener('onAdFailLoad', 	onFailure);
			
			FacebookAds.createNativeAd(this.adId);
		};
		AdsManager.prototype.requestFake = function(){
			var data = {
				adNetwork 	: 'FacebookAds',
				adEvent 	: 'onAdLoaded',
				adRes : {
					ratingScale : 0,
					rating 		: 0,
					title 		: 'Spotify Music',
					coverImage : {
						url 	: 'https://external.xx.fbcdn.net/safe_image.php?d=AQAJmf8B9aH_MaSD&w=796&h=416&url=https%3A%2F%2Fwww.facebook.com%2Fads%2Fimage%2F%3Fd%3DAQJxFWS2MseIY2XPHZ11dKG4Ti9zey92MlXr5rnP0z5E3w-iwKJoEcWLSE6gQwtYmXb_WiZrgYuVkTp5Xy7ZDV25hl-fjsUsaUZc_Yz0VR0cJzeHT1wvH3e3lXy3yDY0nTQcnII4MiTCa1YgBLKoEiw1&crop&sx=0&sy=0&sw=1199&sh=628',
						width 	: 796,
						height 	: 416
					},
					buttonText 	: 'Écouter maintenant',
					body 		: 'Votre playlist s\'inspire de ce que vous aimez & de titres écoutés par des fans comme vous.',
					icon : {
						url 	: 'https://fbexternal-a.akamaihd.net/safe_image.php?d=AQCG0LdpbicsiIFP&w=128&h=128&url=http%3A%2F%2Fis5.mzstatic.com%2Fimage%2Fthumb%2FPurple6%2Fv4%2F3e%2Ffa%2F0c%2F3efa0cd6-c069-77df-2a16-95bcb80ca9b2%2Fmzl.mwbsepby.png%2F0x0ss-85.jpg',
						width 	: 128,
						height 	: 128
					},
					socialContext : 'GRATUIT · Ouvrir dans l’application'
				},
				adType 	: 'native',
				adId 	: this.adId
			};
			return data;
		};

	// Update
		AdsManager.prototype.findClosest = function(ads){
			var closestIndex = -1;
			var minDist = 10 * Styling.getHeight();
			for(var i = 0; i < ads.length; i++){
				var dist = 0;
				if(this.scroller.scrollTop + Styling.getHeight() / 2 < ads[i].offsetTop){
					dist = ads[i].offsetTop - (this.scroller.scrollTop + Styling.getHeight() / 2);
				}
				else if(this.scroller.scrollTop + Styling.getHeight() / 2 > ads[i].offsetTop + Styling.getHeight(ads[i])){
					dist = this.scroller.scrollTop + Styling.getHeight() / 2 - (ads[i].offsetTop + Styling.getHeight(ads[i]));
				}else{
					dist = 0;
				}

				if(dist < minDist){
					minDist = dist;
					closestIndex = i;
				}

				if(dist === 0) break;
			}

			return closestIndex;
		};
		AdsManager.prototype.update = function(){
			if(this.loading) return;

			var ads = this.list.querySelectorAll('.ad-div');
			var closestIndex = this.findClosest(ads);
			if(closestIndex < 0) return;

			var closestAd = ads[closestIndex];

			if(Styling.hasClass(closestAd, 'active')){
				this.updateControl(closestAd);
			}else{
				var activeAd = this.list.querySelector('.ad-div.active');
				if(activeAd) Styling.removeClass(activeAd, 'active');
				Styling.addClass(closestAd, 'active');

				var adsManager = this;
				this.updateContent(closestAd, function(){
					adsManager.updateControl(closestAd);
				});
			}
		};

	// Ad Content
		AdsManager.prototype.updateContent = function(closestAd, callback){
			this.updateContentCallback(this.requestFake(), closestAd);
			var adsManager = this;
			this.request(function(data){adsManager.updateContentCallback(data, closestAd, callback);});
		};
		AdsManager.prototype.updateContentCallback = function(data, closestAd, callback){
			var adData = data.adRes;

			var adTitle = adData.title;
			var adBody 	= adData.body;
			if(adTitle.length > 30){
				adBody = '<span class="sub-title">' + adTitle + '</span><br/>' + '<span>' + adBody + '</span>';
				adTitle = adTitle.substr(0, 30) + '...';
			}

			closestAd.querySelector('.icon-div img').src 			= adData.icon.url;
			closestAd.querySelector('.title-div').innerHTML 		= adTitle;
			closestAd.querySelector('.description-div').innerHTML 	= adBody;
			closestAd.querySelector('.details-div').innerHTML 		= adData.socialContext;
			closestAd.querySelector('.button').innerHTML 			= adData.buttonText;

			var coverDiv 		= closestAd.querySelector('.cover-div');
			var cover 			= closestAd.querySelector('.cover-div img');
			cover.src 			= adData.coverImage.url;
			cover.style.height 	= Styling.getWidth(coverDiv) * adData.coverImage.height / adData.coverImage.width;

			if(callback) callback();
		};

	// Ad Control
		AdsManager.prototype.updateControl = function(closestAd){
			this.addControlToButton(closestAd);
			//this.addControlToAd(scroller, closestAd);
		};
		AdsManager.prototype.addControlToAd = function(closestAd){
			var position = Styling.getPositionInPage(closestAd);
			var x = position.left;
			var y = position.top;
			var w = position.width;
			var h = position.height;

			if(window.cordova) FacebookAds.setNativeAdClickArea(this.adId, x, y, w, h);
			//this.addControlVisibility(x, y ,w ,h);
		};
		AdsManager.prototype.addControlToButton = function(closestAd){
			var button = closestAd.querySelector('.button-div');
			var position = Styling.getPositionInPage(button);
			var x = position.left;
			var y = position.top;
			var w = position.width;
			var h = position.height;

			if(window.cordova) FacebookAds.setNativeAdClickArea(this.adId, x, y, w, h);
			// this.addControlVisibility(x, y ,w ,h);
		};
		AdsManager.prototype.addControlVisibility = function(x, y, w , h){
			console.log(x, y, w, h);
			var adPosition = document.getElementById('ad-position');
				adPosition.style.left 	= x + 'px';
				adPosition.style.top 	= y + 'px';
				adPosition.style.width 	= w + 'px';
				adPosition.style.height = h + 'px';
		};

	// Revoke
		AdsManager.prototype.revoke = function(){
			var activeAd = this.list.querySelector('.ad-div.active');
			if(activeAd) Styling.removeClass(activeAd, 'active');
			if(window.cordova){
				FacebookAds.setNativeAdClickArea(this.adId, 0, 0, 0, 0);
				FacebookAds.removeNativeAd(this.adId);
			}
		};
		AdsManager.prototype.removeAll = function(){
			var ads = this.list.querySelectorAll('.ad-div');
			for(var i = 0; i < ads.length; i++) ads[i].parentNode.removeChild(ads[i]);
		};

	return AdsManager;
});